CREATE TABLE `booking` (
  `id` varchar(36) NOT NULL,
  `mobile_no` varchar(10) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `peoples` int NOT NULL,
  `status` varchar(10) NOT NULL COMMENT 'Booking/Cancel',
  `schedule` date NOT NULL,
  `note` varchar(255) NULL COMMENT 'Any remark',
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;