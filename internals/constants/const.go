package constants

var (
	MAX_PEOPLE_OF_TABLE = 4
	DATE_LAYOUT         = "2006-01-02"
	STATUS_ACTIVE       = "Active"
	STATUS_INACTIVE     = "Inactive"
	STATUS_BOOKING      = "Booking"
	STATUS_CANCEL       = "Cancel"
)
