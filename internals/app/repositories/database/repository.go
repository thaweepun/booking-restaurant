package repositories

import "booking-restaurant/internals/app/repositories/database/entities"

type RepositoryDB interface {
	// Table Info
	CreateTableInfo(tableInfo []entities.TableInfoEntity) error
	CountTableInfo() (int64, error)
	GetTableInfo() ([]entities.TableInfoEntity, error)
	GetTableBooking(schedule string) (map[string]string, error)

	// Manage Reserve
	CreateReserve(reserveHeader entities.BookingEntity, reserveDetails []entities.BookingDetailsEntity) error
	GetReserveInfo(mobileNo, status string) (entities.BookingEntity, error)
	UpdateReserve(mobileNo, status string) error
}
