package config

import (
	"log"
	"strings"

	"github.com/spf13/viper"
)

type (
	AppConfig struct {
		Address string `mapstructure:"addr"`
		Mysql   `mapstructure:"mysql"`
	}

	Mysql struct {
		DBName   string `mapstructure:"db"`
		Addr     string `mapstructure:"addr"`
		Username string `mapstructure:"username"`
		Password string `mapstructure:"password"`
	}
)

func ReadConfig() (AppConfig, error) {
	var config AppConfig

	viper.SetDefault("config.path", "configs")
	err := viper.BindEnv("config.path", "CONFIG_PATH")
	if err != nil {
		return config, err
	}

	viper.SetConfigName("config")                       // name of config file (without extension)
	viper.SetConfigType("yaml")                         // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(viper.GetString("config.path")) // optionally look for config in the working directory
	err = viper.ReadInConfig()                          // Find and read the config file
	if err != nil {                                     // Handle errors reading the config file
		log.Printf("warning: %s \n", err)
		return config, err
	}

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err = viper.Unmarshal(&config)

	return config, err
}
