package models

import "time"

type ReserveTableRequest struct {
	MobileNo     string  `json:"mobileNo" validate:"nonnil"`
	CustomerName string  `json:"customerName" validate:"nonnil"`
	Peoples      int     `json:"peoples" validate:"nonnil,nonzero"`
	Schedule     string  `json:"schedule" validate:"nonnil"`
	Note         *string `json:"note"`
}

type ReserveTableResponse struct {
	MobileNo       string                `json:"mobileNo"`
	CustomerName   string                `json:"customerName"`
	Peoples        int                   `json:"peoples"`
	Schedule       time.Time             `json:"schedule"`
	Status         string                `json:"status"`
	Note           *string               `json:"note"`
	ReserveTable   int                   `json:"reserveTable"`
	RemainTable    int                   `json:"remainTable"`
	BookingDetails []ReserveTableDetails `json:"bookingDetails"`
	CreateAt       time.Time             `json:"createAt"`
}

type ReserveTableDetails struct {
	TableCode string `json:"code"`
	Name      string `json:"name"`
	Seater    int    `json:"seater"`
}

type CancelReserveTableRequest struct {
	MobileNo     string `json:"mobileNo" validate:"nonnil"`
	CustomerName string `json:"customerName" validate:"nonnil"`
}

type CancelReserveTableResponse struct {
	MobileNo     string    `json:"mobileNo"`
	CustomerName string    `json:"customerName"`
	Peoples      int       `json:"peoples"`
	Schedule     time.Time `json:"schedule"`
	Status       string    `json:"status"`
	Note         *string   `json:"note"`
	RemainTable  int       `json:"remainTable"`
	UpdateAt     time.Time `json:"updateAt"`
}
