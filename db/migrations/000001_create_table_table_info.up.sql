CREATE TABLE `table_info` (
  `id` varchar(36) NOT NULL,
  `table_code` varchar(10) NOT NULL,
  `table_name` varchar(50) DEFAULT NULL,
  `no_of_guest` int NOT NULL COMMENT 'Seater',
  `status` varchar(10) NOT NULL COMMENT 'Active/Inactive',
  `note` varchar(255) DEFAULT NULL COMMENT 'Any remark',
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE,
  UNIQUE KEY `table_code` (`table_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci; 