package services

import (
	"booking-restaurant/internals/app/models"
	repositories "booking-restaurant/internals/app/repositories/database"
	"booking-restaurant/internals/app/repositories/database/entities"
	"booking-restaurant/internals/constants"
	"time"

	"github.com/google/uuid"
)

type ManageReserveService interface {
	ReserveTable(mobileNo, customerName, schedule string, peoples int, note *string) (*models.ReserveTableResponse, error)
	CancelReserveTable(mobileNo, customerName string) (*models.CancelReserveTableResponse, error)
}

type manageReserveService struct {
	repoDB repositories.RepositoryDB
}

func NewManageReserveService(repoDB repositories.RepositoryDB) ManageReserveService {
	return manageReserveService{repoDB}
}

func (s manageReserveService) ReserveTable(mobileNo, customerName, schedule string, peoples int, note *string) (*models.ReserveTableResponse, error) {
	//TODO: Business Logic Here
	var reserveInfo entities.BookingEntity
	var reserveDetailsInfo []entities.BookingDetailsEntity
	var err error
	var tablesCount int

	// Check Initialze Table Info
	count, err := s.repoDB.CountTableInfo()
	if err != nil {
		return nil, err
	}
	if count == 0 {
		return nil, constants.ErrTableNotInitial
	}

	reserveInfo, err = s.repoDB.GetReserveInfo(mobileNo, constants.STATUS_BOOKING)
	if err != nil {
		return nil, err
	}

	if reserveInfo.ID == "" {
		// // Calculate Booking Tables From People
		p := peoples
		for (p > 0) && (p <= constants.MAX_PEOPLE_OF_TABLE || (p/constants.MAX_PEOPLE_OF_TABLE) > 0) {
			tablesCount += 1
			p -= constants.MAX_PEOPLE_OF_TABLE
		}

		// Init Date To Booking Header
		scheduleDate, err := time.Parse(constants.DATE_LAYOUT, schedule)
		if err != nil {
			return nil, err
		}

		// Get Table Info Available
		tableAvailable, err := s.getTableAvailable(schedule)
		if err != nil {
			return nil, err
		}

		if len(tableAvailable) >= tablesCount {
			id := uuid.New().String()
			reserveInfo = entities.BookingEntity{
				ID:           id,
				MobileNo:     mobileNo,
				CustomerName: customerName,
				Peoples:      peoples,
				Status:       constants.STATUS_BOOKING,
				Schedule:     scheduleDate,
				Note:         *note,
				CreateAt:     time.Now(),
				UpdateAt:     time.Now(),
			}

			// Init Data To Booking Details
			var seater int
			p = peoples
			for i := 0; i < tablesCount; i++ {

				if p > constants.MAX_PEOPLE_OF_TABLE {
					seater = constants.MAX_PEOPLE_OF_TABLE
				} else {
					seater = p
				}
				p -= constants.MAX_PEOPLE_OF_TABLE

				reserveDetailsInfo = append(reserveDetailsInfo, entities.BookingDetailsEntity{
					ID:        uuid.New().String(),
					BookingID: id,
					TableCode: tableAvailable[i].Code,
					Seater:    seater,
					CreateAt:  time.Now(),
					UpdateAt:  time.Now(),
				})
			}

			err = s.repoDB.CreateReserve(reserveInfo, reserveDetailsInfo)
			if err != nil {
				return nil, err
			}
		} else {
			return nil, constants.ErrTableNotEnough
		}
	} else {
		return nil, constants.ErrReserveExists
	}

	// Get Table Info Available
	tableAvailable, err := s.getTableAvailable(schedule)
	if err != nil {
		return nil, err
	}

	// Mapping To Model Response Details
	var details []models.ReserveTableDetails
	for _, r := range reserveDetailsInfo {
		details = append(details, models.ReserveTableDetails{
			TableCode: r.TableCode,
			Name:      "Table No." + r.TableCode,
			Seater:    r.Seater,
		})
	}

	// Mapping To Model Response
	results := models.ReserveTableResponse{
		MobileNo:       reserveInfo.MobileNo,
		CustomerName:   reserveInfo.CustomerName,
		Peoples:        reserveInfo.Peoples,
		Schedule:       reserveInfo.Schedule,
		Status:         reserveInfo.Status,
		Note:           &reserveInfo.Note,
		ReserveTable:   len(details),
		RemainTable:    len(tableAvailable),
		BookingDetails: details,
		CreateAt:       reserveInfo.CreateAt,
	}

	return &results, nil
}

func (s manageReserveService) CancelReserveTable(mobileNo, customerName string) (*models.CancelReserveTableResponse, error) {
	//TODO: Business Logic Here
	var reserveInfo entities.BookingEntity
	var err error

	// Check Initialze Table Info
	count, err := s.repoDB.CountTableInfo()
	if err != nil {
		return nil, err
	}
	if count == 0 {
		return nil, constants.ErrTableNotInitial
	}

	reserveInfo, err = s.repoDB.GetReserveInfo(mobileNo, constants.STATUS_BOOKING)
	if err != nil {
		return nil, err
	}

	if reserveInfo.ID != "" {
		err := s.repoDB.UpdateReserve(mobileNo, constants.STATUS_CANCEL)
		if err != nil {
			return nil, err
		}
	} else {
		return nil, constants.ErrRecordNotFound
	}

	// Get Table Info Available
	tableAvailable, err := s.getTableAvailable(reserveInfo.Schedule.Format(constants.DATE_LAYOUT))
	if err != nil {
		return nil, err
	}

	// Mapping To Model Response
	results := models.CancelReserveTableResponse{
		MobileNo:     reserveInfo.MobileNo,
		CustomerName: reserveInfo.CustomerName,
		Peoples:      reserveInfo.Peoples,
		Schedule:     reserveInfo.Schedule,
		Status:       constants.STATUS_CANCEL,
		Note:         &reserveInfo.Note,
		RemainTable:  len(tableAvailable),
		UpdateAt:     reserveInfo.UpdateAt,
	}

	return &results, nil
}

func (s manageReserveService) getTableAvailable(schedule string) ([]models.TableInfo, error) {
	// Get All Table Info Active
	tableInfo, err := s.repoDB.GetTableInfo()
	if err != nil {
		return nil, err
	}

	// Get Table Booking
	mapTableBooking, err := s.repoDB.GetTableBooking(schedule)
	if err != nil {
		return nil, err
	}

	// Gen Table Info Available
	var tableAvailable []models.TableInfo
	for _, t := range tableInfo {
		_, ok := mapTableBooking[t.Code]
		if !ok {
			tableAvailable = append(tableAvailable, models.TableInfo{
				Code:      t.Code,
				Name:      t.Name,
				NoOfGuest: t.NoOfGuest,
				Status:    t.Status,
				Note:      t.Note,
			})
		}
	}

	return tableAvailable, nil
}
