package services_test

import (
	repositories "booking-restaurant/internals/app/repositories/database"
	"booking-restaurant/internals/app/repositories/database/entities"
	"booking-restaurant/internals/app/services"
	"testing"

	"github.com/stretchr/testify/mock"
	"gotest.tools/assert"
)

func TestReserveTable(t *testing.T) {
	type testCase struct {
		name          string
		mobileNo      string
		customerName  string
		peoples       int
		schedule      string
		note          *string
		expectedTable int
	}

	cases := []testCase{
		{name: "Reserve 1 People", mobileNo: "0896269501", customerName: "Mr.A", peoples: 1, schedule: "2023-06-01", note: nil, expectedTable: 1},
		{name: "Reserve 2 Peoples", mobileNo: "0896269502", customerName: "Mr.B", peoples: 2, schedule: "2023-06-01", note: nil, expectedTable: 1},
		{name: "Reserve 3 Peoples", mobileNo: "0896269503", customerName: "Mr.C", peoples: 3, schedule: "2023-06-01", note: nil, expectedTable: 1},
		{name: "Reserve 4 Peoples", mobileNo: "0896269504", customerName: "Mr.D", peoples: 4, schedule: "2023-06-01", note: nil, expectedTable: 1},
		{name: "Reserve 5 Peoples", mobileNo: "0896269505", customerName: "Mr.E", peoples: 5, schedule: "2023-06-01", note: nil, expectedTable: 2},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			// Arrage
			repoDb := repositories.NewRepositoryDBMock()
			repoDb.On("GetReserveInfo", mock.Anything, mock.Anything).Return(entities.BookingEntity{}, nil)
			repoDb.On("CreateReserve", mock.Anything, mock.Anything).Return(nil)
			repoDb.On("CreateTableInfo", mock.Anything).Return(nil)
			repoDb.On("CountTableInfo").Return(int64(5), nil)
			repoDb.On("GetTableInfo").Return([]entities.TableInfoEntity{}, nil)
			repoDb.On("GetTableBooking", mock.Anything).Return(map[string]string{}, nil)

			manageReserveService := services.NewManageReserveService(repoDb)

			// Act
			bookingTables, _ := manageReserveService.ReserveTable(c.mobileNo, c.customerName, c.schedule, c.peoples, c.note)
			expected := c.expectedTable

			// Assert
			if bookingTables != nil {
				assert.Equal(t, expected, len(bookingTables.BookingDetails))
			}
		})
	}
}
