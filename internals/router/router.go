package router

import (
	"booking-restaurant/internals/app/handlers"
	repositories "booking-restaurant/internals/app/repositories/database"
	"booking-restaurant/internals/app/services"
	"booking-restaurant/internals/config"
	"context"
	"log"
	"net/http"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type RouterHandler struct {
	AppConf              config.AppConfig
	Router               *gin.Engine
	DB                   *gorm.DB
	SettingTableHandler  handlers.TableInfoHandler
	ManageReserveHandler handlers.ManageReserveHandler
}

func NewRouterHandler(appConf config.AppConfig, db *gorm.DB) *RouterHandler {
	// Setting Repository
	repoDB := repositories.NewRepositoryDB(db)

	// Setting Service
	tableInfoService := services.NewTableInfoService(repoDB)
	reverveTableService := services.NewManageReserveService(repoDB)

	return &RouterHandler{
		AppConf:              appConf,
		Router:               gin.New(),
		DB:                   db,
		SettingTableHandler:  handlers.NewTableInfoHandler(tableInfoService),
		ManageReserveHandler: handlers.NewManageReserveHandler(reverveTableService),
	}
}

func (rH *RouterHandler) SetRoutes() {
	// Adding API Endpoint and handler here.
	routerGroup := rH.Router.Group("/api/v1")
	routerGroup.POST("/initialAllTables", rH.SettingTableHandler.InitialAllTableInfo)
	routerGroup.POST("/reserveTable", rH.ManageReserveHandler.ReserveTable)
	routerGroup.POST("/cancelReserveTable", rH.ManageReserveHandler.CancelReserveTable)
}

func (rH *RouterHandler) Start() {
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	srv := &http.Server{
		Addr:    rH.AppConf.Address,
		Handler: rH.Router,
	}
	log.Println("Container Start Port ", rH.AppConf.Address)

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	// Listen for the interrupt signal.
	<-ctx.Done()

	// Restore default behavior on the interrupt signal and notify user of shutdown.
	stop()
	log.Println("shutting down gracefully, press Ctrl+C again to force")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown: ", err)
	}
	log.Println("Server exiting")
}
