# Booking Restaurant Step Runing

1. Run Docker Mysql
    - `docker compose up -d`

2. Run SQL Script
    - `make migrate-up`

3. Start App
    - `make run`

4. Call Services Method : POST
    - http://localhost:8888/api/v1/initialAllTables
        ```
        {
            "tables": 5
        }
        ```
    - http://localhost:8888/api/v1/reserveTable
        ```
        {
            "mobileNo": "0896269501",
            "customerName": "Ton",
            "schedule": "2023-06-01",
            "peoples": 2,
            "note": "test"
        }
        ```
    - http://localhost:8888/api/v1/cancelReserveTable
        ```
        {
            "mobileNo": "0896269501",
            "customerName": "Ton"
        }
        ```

5. Check Unit Test & Quality
    - `make check`

# Project Structure [API]
- `cmd/` main application.
    - `cmd/server` default main application to run.
- `configs/` config .env file yaml
- `db/` main application.
    - `db/migrations` store sql generate initial step
    - `db/scripts` store scripts migrations tool up & down
- `internals/` package to be implement. 
    - `internals/app` package to be for app.
        - `internals/app/handlers/{business_domain}` __You should put your handlers controller in here.__
        - `internals/app/repositories/{business_domain}` __You should put your repository connector like database in here.__
        - `internals/app/services/{business_domain}` __You should put your services and business logic in here.__
        - `internals/app/models/{business_domain}` __You should put your struct in here.__
    - `internals/config` config struct
    - `internals/constants` init constants
    - `internals/router` api path