check:
	make runquality
	make runformat
	make runalltests

run:
	go run cmd/server/main.go

runquality:
	golangci-lint run --timeout 5m

runformat:
	go fmt ./...

runalltests:
	go test ./...

runcoverage:
	go test ./... -coverprofile=coverage.out && go tool cover -html=coverage.out

migrate-up:
	@cd db/scripts && ./migrate_up.sh 

migrate-down:
	@cd db/scripts && ./migrate_down.sh 