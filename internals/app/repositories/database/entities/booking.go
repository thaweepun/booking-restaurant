package entities

import "time"

type BookingEntity struct {
	ID           string    `gorm:"column:id;primary_key"`
	MobileNo     string    `gorm:"column:mobile_no"`
	CustomerName string    `gorm:"column:customer_name"`
	Peoples      int       `gorm:"column:peoples"`
	Status       string    `gorm:"column:status"`
	Schedule     time.Time `gorm:"column:schedule"`
	Note         string    `gorm:"column:note"`
	CreateAt     time.Time `gorm:"column:create_at"`
	UpdateAt     time.Time `gorm:"column:update_at"`
}

func (BookingEntity) TableName() string {
	return "booking"
}
