package models

import "time"

type InitialAllTablesRequest struct {
	Tables uint `json:"tables" validate:"nonnil,nonzero"`
}

type InitialAllTablesResponse struct {
	Code      string    `json:"code"`
	Name      string    `json:"name"`
	NoOfGuest int       `json:"noOfGuest"`
	Note      *string   `json:"note"`
	CreateAt  time.Time `json:"createAt"`
}

type TableInfo struct {
	Code      string
	Name      string
	NoOfGuest int
	Status    string
	Note      *string
}
