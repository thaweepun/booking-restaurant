package services

import (
	"booking-restaurant/internals/app/models"
	repositories "booking-restaurant/internals/app/repositories/database"
	"booking-restaurant/internals/app/repositories/database/entities"
	"booking-restaurant/internals/constants"
	"strconv"
	"time"

	"github.com/google/uuid"
)

type TableInfoService interface {
	InitialAllTables(tables int) ([]models.InitialAllTablesResponse, error)
}

type tableInfoService struct {
	repoDB repositories.RepositoryDB
}

func NewTableInfoService(repoDB repositories.RepositoryDB) TableInfoService {
	return tableInfoService{repoDB}
}

func (s tableInfoService) InitialAllTables(tables int) ([]models.InitialAllTablesResponse, error) {
	//TODO: Business Logic Here
	// Check Initialze Table Info
	count, err := s.repoDB.CountTableInfo()
	if err != nil {
		return nil, err
	}
	if count > 0 {
		return nil, constants.ErrRecordsExists
	}

	// Init Data To Table Info
	tableInfo := []entities.TableInfoEntity{}
	for i := 1; i <= tables; i++ {
		tableInfo = append(tableInfo, entities.TableInfoEntity{
			ID:        uuid.New().String(),
			Code:      strconv.Itoa(i),
			Name:      "Table No." + strconv.Itoa(i),
			NoOfGuest: constants.MAX_PEOPLE_OF_TABLE,
			Status:    constants.STATUS_ACTIVE,
			CreateAt:  time.Now(),
			UpdateAt:  time.Now(),
		})
	}

	// Init Data To Table Info
	err = s.repoDB.CreateTableInfo(tableInfo)
	if err != nil {
		return nil, err
	}

	// Get Data From Table Info Reponse
	resTableInfos, err := s.repoDB.GetTableInfo()
	if err != nil {
		return nil, err
	}

	// Mapping To Model Response
	var results []models.InitialAllTablesResponse
	for _, r := range resTableInfos {
		results = append(results, models.InitialAllTablesResponse{
			Code:      r.Code,
			Name:      r.Name,
			NoOfGuest: r.NoOfGuest,
			Note:      r.Note,
			CreateAt:  r.CreateAt,
		})
	}

	return results, nil
}
