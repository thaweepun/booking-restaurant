package repositories

import (
	"booking-restaurant/internals/app/repositories/database/entities"

	"github.com/stretchr/testify/mock"
)

type repositoryDBMock struct {
	mock.Mock
}

func NewRepositoryDBMock() *repositoryDBMock {
	return &repositoryDBMock{}
}

// Table Info
func (m *repositoryDBMock) CreateTableInfo(tableInfo []entities.TableInfoEntity) error {
	args := m.Called(tableInfo)
	return args.Error(0)
}

func (m *repositoryDBMock) CountTableInfo() (int64, error) {
	args := m.Called()
	return args.Get(0).(int64), args.Error(1)
}

func (m *repositoryDBMock) GetTableInfo() ([]entities.TableInfoEntity, error) {
	args := m.Called()
	return args.Get(0).([]entities.TableInfoEntity), args.Error(1)
}

func (m *repositoryDBMock) GetTableBooking(schedule string) (map[string]string, error) {
	args := m.Called(schedule)
	return args.Get(0).(map[string]string), args.Error(1)
}

// Manage Reserve
func (m *repositoryDBMock) CreateReserve(reserveHeader entities.BookingEntity, reserveDetails []entities.BookingDetailsEntity) error {
	args := m.Called(reserveHeader, reserveDetails)
	return args.Error(0)
}

func (m *repositoryDBMock) GetReserveInfo(mobileNo, status string) (entities.BookingEntity, error) {
	args := m.Called(mobileNo)
	return args.Get(0).(entities.BookingEntity), args.Error(1)
}

func (m *repositoryDBMock) UpdateReserve(mobileNo, status string) error {
	args := m.Called(mobileNo, status)
	return args.Error(0)
}
