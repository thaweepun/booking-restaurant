package services_test

import (
	repositories "booking-restaurant/internals/app/repositories/database"
	"booking-restaurant/internals/app/repositories/database/entities"
	"booking-restaurant/internals/app/services"
	"strconv"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"
	"gotest.tools/assert"
)

func TestInitialAllTable(t *testing.T) {
	type testCase struct {
		name     string
		tables   int64
		expected int64
	}

	cases := []testCase{
		{name: "Create 1 Table", tables: 1, expected: 1},
		{name: "Create 2 Tables", tables: 2, expected: 2},
		{name: "Create 3 Tables", tables: 3, expected: 3},
		{name: "Create 4 Tables", tables: 4, expected: 4},
		{name: "Create 5 Tables", tables: 5, expected: 5},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			// Prepare Data
			var dataTest []entities.TableInfoEntity
			for i := 1; i <= int(c.tables); i++ {
				dataTest = append(dataTest, entities.TableInfoEntity{
					ID:        uuid.New().String(),
					Code:      strconv.Itoa(i),
					Name:      "Table No." + strconv.Itoa(i),
					NoOfGuest: 4,
					Status:    "Active",
				})
			}

			// Arrage
			repoDb := repositories.NewRepositoryDBMock()
			repoDb.On("CountTableInfo").Return(int64(0), nil)
			repoDb.On("CreateTableInfo", mock.Anything).Return(nil)
			repoDb.On("GetTableInfo").Return(dataTest, nil)

			tableInfoService := services.NewTableInfoService(repoDb)

			// Act
			initTables, _ := tableInfoService.InitialAllTables(int(c.tables))
			expected := c.expected

			// Assert
			assert.Equal(t, expected, int64(len(initTables)))
		})
	}
}
