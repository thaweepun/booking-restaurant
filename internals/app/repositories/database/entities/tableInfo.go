package entities

import "time"

type TableInfoEntity struct {
	ID        string    `gorm:"column:id;primary_key"`
	Code      string    `gorm:"column:table_code"`
	Name      string    `gorm:"column:table_name"`
	NoOfGuest int       `gorm:"column:no_of_guest"`
	Status    string    `gorm:"column:status"`
	Note      *string   `gorm:"column:note"`
	CreateAt  time.Time `gorm:"column:create_at"`
	UpdateAt  time.Time `gorm:"column:update_at"`
}

func (TableInfoEntity) TableName() string {
	return "table_info"
}
