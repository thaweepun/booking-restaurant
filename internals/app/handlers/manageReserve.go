package handlers

import (
	"booking-restaurant/internals/app/models"
	"booking-restaurant/internals/app/services"
	"booking-restaurant/internals/constants"
	"net/http"

	"github.com/gin-gonic/gin"
	"gopkg.in/validator.v2"
)

type ManageReserveHandler interface {
	ReserveTable(c *gin.Context)
	CancelReserveTable(c *gin.Context)
}

type manageReserveHandler struct {
	manageReserveService services.ManageReserveService
}

func NewManageReserveHandler(manageReserveService services.ManageReserveService) ManageReserveHandler {
	return manageReserveHandler{manageReserveService}
}

func (h manageReserveHandler) ReserveTable(c *gin.Context) {
	// init Request Data
	req := models.ReserveTableRequest{}
	if err := c.Bind(&req); err != nil {
		c.JSON(http.StatusBadRequest, constants.ErrorContext{
			HttpStatus: http.StatusBadRequest,
			Error:      constants.ErrBadRequestJSONBinding.Error(),
		})
		return
	}

	// Validate Data Request Input
	if err := validator.Validate(&req); err != nil {
		c.JSON(http.StatusBadRequest, constants.ErrorContext{
			HttpStatus: http.StatusBadRequest,
			Error:      constants.ErrBadRequestValidateData.Error(),
		})
		return
	}

	// Business Logic
	resp, err := h.manageReserveService.ReserveTable(req.MobileNo, req.CustomerName, req.Schedule, req.Peoples, req.Note)
	if err != nil {
		c.JSON(http.StatusInternalServerError, constants.ErrorContext{
			HttpStatus: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	//Return response
	c.JSON(http.StatusOK, resp)
}

func (h manageReserveHandler) CancelReserveTable(c *gin.Context) {
	// init Request Data
	req := models.CancelReserveTableRequest{}
	if err := c.Bind(&req); err != nil {
		c.JSON(http.StatusBadRequest, constants.ErrorContext{
			HttpStatus: http.StatusBadRequest,
			Error:      constants.ErrBadRequestJSONBinding.Error(),
		})
		return
	}

	// Validate Data Request Input
	if err := validator.Validate(&req); err != nil {
		c.JSON(http.StatusBadRequest, constants.ErrorContext{
			HttpStatus: http.StatusBadRequest,
			Error:      constants.ErrBadRequestValidateData.Error(),
		})
		return
	}

	// Business Logic
	resp, err := h.manageReserveService.CancelReserveTable(req.MobileNo, req.CustomerName)
	if err != nil {
		c.JSON(http.StatusInternalServerError, constants.ErrorContext{
			HttpStatus: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	//Return response
	c.JSON(http.StatusOK, resp)
}
