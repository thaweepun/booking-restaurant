CREATE TABLE `booking_details` (
  `id` varchar(36) NOT NULL,
  `booking_id` varchar(36) NOT NULL,
  `table_code` varchar(10) NOT NULL,
  `seater` int NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;