package constants

import "errors"

type ErrorContext struct {
	HttpStatus int
	Error      string
}

var (
	ErrBadRequestJSONBinding  = errors.New("BadRequest Error JSON Binding")
	ErrBadRequestValidateData = errors.New("BadRequest Error Validate Data")
	ErrRecordsExists          = errors.New("Records Exists")
	ErrReserveExists          = errors.New("Reserve Exists")
	ErrRecordNotFound         = errors.New("Record Not Found")
	ErrTableNotEnough         = errors.New("Table Not Enough")
	ErrTableNotInitial        = errors.New("Table Not Initial")
)
