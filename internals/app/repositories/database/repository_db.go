package repositories

import (
	"booking-restaurant/internals/app/repositories/database/entities"
	"booking-restaurant/internals/constants"
	"errors"
	"time"

	"gorm.io/gorm"
)

type repositoryDB struct {
	db *gorm.DB
}

func NewRepositoryDB(db *gorm.DB) RepositoryDB {
	return &repositoryDB{db}
}

func (r *repositoryDB) CreateTableInfo(tableInfo []entities.TableInfoEntity) error {
	return r.db.Model(entities.TableInfoEntity{}).Create(tableInfo).Error
}

func (r *repositoryDB) CountTableInfo() (int64, error) {
	var result int64
	return result, r.db.Model(entities.TableInfoEntity{}).Count(&result).Error
}

func (r *repositoryDB) GetTableInfo() ([]entities.TableInfoEntity, error) {
	var result []entities.TableInfoEntity
	return result, r.db.Model(entities.TableInfoEntity{}).Where("status = ?", constants.STATUS_ACTIVE).
		Order("table_code").
		Find(&result).Error
}

func (r *repositoryDB) GetTableBooking(schedule string) (map[string]string, error) {
	var tableInfo []entities.TableInfoEntity

	err := r.db.Model(entities.TableInfoEntity{}).
		Joins("left join booking_details on booking_details.table_code = table_info.table_code").
		Joins("left join booking on booking.id = booking_details.booking_id and booking.status = ?", constants.STATUS_BOOKING).
		Where("booking.schedule = ? and table_info.status = ?", schedule, constants.STATUS_ACTIVE).
		Group("table_info.table_code").
		Order("table_info.table_code").
		Scan(&tableInfo).Error
	if err != nil {
		return nil, err
	}

	result := make(map[string]string)
	for _, t := range tableInfo {
		result[t.Code] = t.Name
	}

	return result, nil
}

func (r *repositoryDB) CreateReserve(reserveHeader entities.BookingEntity, reserveDetails []entities.BookingDetailsEntity) error {
	tx := r.db.Begin()

	err := tx.Model(entities.BookingEntity{}).Create(reserveHeader).Error
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Model(entities.BookingDetailsEntity{}).Create(reserveDetails).Error
	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

func (r *repositoryDB) GetReserveInfo(mobileNo, status string) (entities.BookingEntity, error) {
	var result entities.BookingEntity
	err := r.db.Model(entities.BookingEntity{}).Where("(mobile_no = ?) and (status = ?)", mobileNo, status).First(&result).Error
	if !errors.Is(gorm.ErrRecordNotFound, err) {
		return result, err
	}

	return result, nil
}

func (r *repositoryDB) UpdateReserve(mobileNo, status string) error {
	updatedColumns := make(map[string]interface{})
	updatedColumns["status"] = status
	updatedColumns["update_at"] = time.Now()

	return r.db.Model(entities.BookingEntity{}).Where("mobile_no = ?", mobileNo).Updates(updatedColumns).Error
}
