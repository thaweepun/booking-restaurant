package handlers

import (
	"booking-restaurant/internals/app/models"
	"booking-restaurant/internals/app/services"
	"booking-restaurant/internals/constants"
	"net/http"

	"github.com/gin-gonic/gin"
	"gopkg.in/validator.v2"
)

type TableInfoHandler interface {
	InitialAllTableInfo(c *gin.Context)
}

type tableInfoHandler struct {
	tableInfoService services.TableInfoService
}

func NewTableInfoHandler(tableInfoService services.TableInfoService) TableInfoHandler {
	return tableInfoHandler{tableInfoService}
}

func (h tableInfoHandler) InitialAllTableInfo(c *gin.Context) {
	// init Request Data
	req := models.InitialAllTablesRequest{}
	if err := c.Bind(&req); err != nil {
		c.JSON(http.StatusBadRequest, constants.ErrorContext{
			HttpStatus: http.StatusBadRequest,
			Error:      constants.ErrBadRequestJSONBinding.Error(),
		})
		return
	}

	// Validate Data Request Input
	if err := validator.Validate(&req); err != nil {
		c.JSON(http.StatusBadRequest, constants.ErrorContext{
			HttpStatus: http.StatusBadRequest,
			Error:      constants.ErrBadRequestValidateData.Error(),
		})
		return
	}

	// Business Logic
	resp, err := h.tableInfoService.InitialAllTables(int(req.Tables))
	if err != nil {
		c.JSON(http.StatusInternalServerError, constants.ErrorContext{
			HttpStatus: http.StatusInternalServerError,
			Error:      err.Error(),
		})
		return
	}

	//Return response
	c.JSON(http.StatusOK, resp)
}
