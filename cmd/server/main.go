package main

import (
	"booking-restaurant/internals/config"
	"booking-restaurant/internals/router"
	"fmt"
	"log"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	// Init Time Zone
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		log.Printf("error loading location 'Asia/Bangkok': %v\n", err)
	}
	time.Local = ict

	// Read Configuration
	appConf, err := config.ReadConfig()
	if err != nil {
		log.Fatalln(err)
	}

	// Init Database
	db, err := initDatabase(appConf.Mysql.Addr, appConf.Mysql.DBName, appConf.Mysql.Username, appConf.Mysql.Password)
	if err != nil {
		log.Fatalln(err)
	}

	// Start Server Container & Router
	router := router.NewRouterHandler(appConf, db)
	router.SetRoutes()
	router.Start()
}

func initDatabase(addr, dbname, username, password string) (*gorm.DB, error) {
	dial := mysql.Open(fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&parseTime=True&loc=Asia%%2FBangkok", username, password, addr, dbname))

	db, err := gorm.Open(dial, &gorm.Config{})
	if err != nil {
		return nil, err
	}

	return db, nil
}
