package entities

import "time"

type BookingDetailsEntity struct {
	ID        string    `gorm:"column:id;primary_key"`
	BookingID string    `gorm:"column:booking_id"`
	TableCode string    `gorm:"column:table_code"`
	Seater    int       `gorm:"column:seater"`
	CreateAt  time.Time `gorm:"column:create_at"`
	UpdateAt  time.Time `gorm:"column:update_at"`
}

func (BookingDetailsEntity) TableName() string {
	return "booking_details"
}
